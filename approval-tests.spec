%global debug_package %{nil}

Name:    approval-tests
Summary: Approval testing framework
Version: 10.12.1
Release: 1%{?dist}
License: Apache 2.0
URL:     https://github.com/approvals/ApprovalTests.cpp
Source0: %{url}/releases/download/v.%{version}/ApprovalTests.v.%{version}.hpp

%description
Also known as Golden Master Tests or Snapshot Testing, Approval Tests are an alternative to asserts.

%package        devel
Summary:        Development files for %{name}
Requires:       catch-devel

%description    devel
Approval Tests are an alternative way to write asserts in your test code.

%prep
cp %{SOURCE0} %{_builddir}

%build
# nothing to do

%install
mkdir -p %{buildroot}%{_includedir}/%{name}
install -p -m 0644 %{_builddir}/ApprovalTests.v.%{version}.hpp %{buildroot}%{_includedir}/%{name}/ApprovalTests.hpp

%files devel
%{_includedir}/%{name}/ApprovalTests.hpp

%changelog
* Fri Feb 18 2022 Milivoje Legenovic <milivoje.legenovic@profidata.com> - 10.12.1-1
- Update to upstream version
- xdev-6 build

* Wed May 05 2021 Levent Demirörs <levent.demiroers@profidata.com> - 10.9.1-1
- Update to 10.9.1
